# IBM ML with Python

k-Nearest Neighbour, Decision Tree, Support Vector Machine and Logistic Regression classification algorithms on the given data. The results is reported as the accuracy of each classifier, using the following metrics when these are applicable: Jaccard